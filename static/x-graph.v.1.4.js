/**
 * Created by Core.Today on 2017-07-04.
 */
var graph = Viva.Graph.graph(),
    webglGraphics = Viva.Graph.View.webglGraphics(),
    layout = Viva.Graph.Layout.forceDirected(graph,{
        springLength: 8,
        springCoeff: 0.00005,
        dragCoeff: 0.1,
        gravity: -9
    })
    events = Viva.Graph.webglInputEvents(webglGraphics, graph),
    renderer = Viva.Graph.View.renderer(graph,{
        layout: layout,
        graphics : webglGraphics,
        container : document.getElementById('render')
    });
graph.labels = Object.create(null);

var colors = [
    0x1f77b4ff, 0xaec7e8ff,
    0xff7f0eff, 0xffbb78ff,
    0x2ca02cff, 0x98df8aff,
    0xd62728ff, 0xff9896ff,
    0x9467bdff, 0xc5b0d5ff,
    0x8c564bff, 0xc49c94ff,
    0xe377c2ff, 0xf7b6d2ff,
    0x7f7f7fff, 0xc7c7c7ff,
    0xbcbd22ff, 0xdbdb8dff,
    0x17becfff, 0x9edae5ff
];

var domain = "http://52.79.177.143:8081/g/";
var primaryNodeName = $('.badge-info')[0].innerText;
var zeroPosition = null;

!function () {
    $('#g-loading').css('visibility', 'visible');
    var url = domain + primaryNodeName;
    $.getJSON(url, function(data){
        $('#g-loading').css('visibility', 'hidden');
        console.log(data); /* getJSON으로 받은 데이터 */
        data.node.isPinned = true; /* primary node를 고정 */
        zeroPosition = data.node.y;
        graph.addNode(data.node.i, data.node); /* 그래프에 primary node를 정보와 함께 삽입 */
        console.log(data.node)
        addPLabel(data.node)

        // primary node와 1차적으로 연결 된 node들을 그려줌
        for(nodesInfo in data.nodes){
            // console.log(data.nodes[nodesInfo]);
            graph.addNode(data.nodes[nodesInfo].i, data.nodes[nodesInfo]);

        }

        // 불러온 node들 전체를 연결한다.
        for(links in data.edges){
            // console.log(links);
            graph.addLink(data.edges[links].s, data.edges[links].t);
        }


    })
}();

// 내 눈에만 보이는 최고의 코드.
// node를 언제 불러왔는지 상관 없이, graph에 그리는 시점에 바로 크기와 색상, x-position을 정해주는 코드
// 새로 그려지는 node들의 정보만 get하며, 추가로 각각의 node에 정보를 set 할 수 있게 해 준다.
// 기존의 x-graph.js에서 insertDataForEachNode 함수에 해당하는 부분.
// 짧게 만들어서 기분 좋음.
webglGraphics.node(function (node) {
    // console.log(node)
    if(node.data.k !== undefined){
        if(node.data.k === 'j'){
            return Viva.Graph.View.webglSquare(25, 0xFF0048FF);
        } else if(node.data.k === 'p'){
            if(node.data.y !== ''){
                node.xPosition = node.data.y - zeroPosition
            }
            return Viva.Graph.View.webglSquare(25, 0x009DFFFF);
        }
    }

});

appendLabel();


events.click(function(node){
    console.log('click');
    // console.log(node);
    addLabel(node)
}).dblClick(function (node) {
    console.log('dblClick');
    !function () {
        $('#g-loading').css('visibility', 'visible');
        var url = domain + node.data.n; /* node.data.n == 더블클릭한 노드의 이름 */
        $.getJSON(url, function(data){
            // console.log(data); /* getJSON으로 받은 데이터 */
            $('#g-loading').css('visibility', 'hidden');
            // primary node와 1차적으로 연결 된 node들을 graph에 info와 함께 삽입
            for(nodesInfo in data.nodes){
                // console.log(data.nodes[nodesInfo])
                graph.addNode(data.nodes[nodesInfo].i, data.nodes[nodesInfo])

            }

            // 불러온 node들 전체를 연결한다.
            for(links in data.edges){
                // console.log(links)
                graph.addLink(data.edges[links].s, data.edges[links].t)
            }


        })
    }();
})

renderer.run();


$('#xAlign').click(xAlign);
$('#webAlign').click(webAlign);
$('#show').on('click', function () {
    graph.forEachNode(function (node) {
        // console.log(node)
        if(node.data.n !== undefined){
            addLabel(node)
        }
    });
    $('.node-label').show();
    console.log("show")
});
$('#hide').on('click', function () {
    $('.node-label').hide();
    console.log("hide")
});
$('#degree').click(degreeSize);
$('#random').click(randomNodeColor);
$('#origin').click(origin);
$('#add_line').click(addLine);
$('#lineRemove').click(lineRemove);
$('#lineRedraw').click(lineRedraw);
$('#newLineClean').click(newLineClean);

function newLineClean() {
    for (var i=0; i<newLineArray.length;i++){
        // console.log(newLineArray[i])
        graph.removeLink(newLineArray[i])
    }
    newLineArray = []
}

function lineRemove() {
    // if(newLineArray.length !== 0){
    //     newLineArray = []
    // }
    for (var i=0; i<newLineArray.length;i++){
        // console.log(newLineArray[i])
        graph.removeLink(newLineArray[i])
    }
    console.log(newLineArray)
}

var tempArray = []
function lineRedraw() {
    for (var i=0; i<newLineArray.length; i++){
        // console.log(newLineArray[i].id);
        // console.log(graph.addLink(newLineArray[i].id))
        var reLinkOj = graph.addLink(newLineArray[i].fromId, newLineArray[i].toId);
        var reLinkid = reLinkOj.id;
        tempArray.push(reLinkOj)
        // console.log(reLinkid)
        webglGraphics.getLinkUI(reLinkid).color = 0xFF0048FF
    }
    newLineArray = tempArray;
    tempArray = [];
    console.log(newLineArray)
}


// node가 가진 link의 수에 비례하여 node의 크기를 변경 해주는 함수
function degreeSize(){
    graph.forEachNode(function (node) {
      // console.log(node)
      // console.log(webglGraphics.getNodeUI(node.id));
      webglGraphics.getNodeUI(node.id).size = 5 * Math.sqrt(node.links.length)
    })
}

//random으로 node의 color를 지정해주는 함수
function randomNodeColor() {
    graph.forEachNode(function (node) {
        // console.log(webglGraphics.getNodeUI(node.id));
        webglGraphics.getNodeUI(node.id).color = colors[(Math.random() * colors.length) << 0]
    })
}

//graph를 제일 처음 그렸던 모양으로 되돌려주는 함수
function origin() {
    graph.forEachNode(function (node) {
        webglGraphics.getNodeUI(node.id).size = 25
        if(node.data.k === "p")
            webglGraphics.getNodeUI(node.id).color = 10354687
        else if(node.data.k === "j")
            webglGraphics.getNodeUI(node.id).color = 4278208767
    })
}

var newLineArray = []

// line을 추가하는 함수.
// 현재, 라인을 그릴 때 마다 node id를 array에 새로 push하기 때문에, 이를 분리 할 필요가 있음.
function addLine() {
    var nodeIdArray = []
    graph.forEachNode(function (node) {
        nodeIdArray.push(node.id)
    })
    // console.log(nodeIdArray)
    var a = nodeIdArray[Math.random()*nodeIdArray.length << 0];
    var b = nodeIdArray[Math.random()*nodeIdArray.length << 0];

    // 새로운 선을 그릴 때, 색도 같이 집어 넣어주는 함수.
    // graph.addLink()에서 선을 그리고, .id를 통해 그리는 선의 id를 빼온다.
    // 위에서 나온 id를 그대로 getLinkUI에 인수로 집어 넣고, .color로 color 값을 가져온다.
    // 가져온 color 값에 원하는 색코드를 대입 해 주면 끗.
    var newLineOj =  graph.addLink(a, b)
    var newLineId = newLineOj.id
    newLineArray.push(newLineOj)
    console.log(newLineArray)
    webglGraphics.getLinkUI(newLineId).color = 0xFF0048FF
}


function xAlign (){
    webglGraphics.placeNode(function (ui, pos) {
        //section stert
        //xAlign에서 x축에 node를 xPosition의 값을 가져와서 대입 해주는 부분.
        if(ui.node.data.y !== ""){
            if(ui.node.xPosition !== undefined && ui.node.xPosition < 100){
                ui.position.x =  (ui.node.xPosition)*40;
            }else if ( ui.node.xPosition > 100 ){
                ui.position.x = 1500;
            }
        }
        //section end

        //section start
        //xAlign에서 node 움직임에 따라 Label을 옮겨주는 부분.
        var domPos = {
            x : pos.x,
            y : pos.y
        };
        webglGraphics.transformGraphToClientCoordinates(domPos);

        var nodeId = ui.node.id;
        if (graph.labels[nodeId] !== undefined & graph.getNode(nodeId).data !== undefined){
            var labelStyle = graph.labels[nodeId].style;
            labelStyle.left = domPos.x + 'px';
            labelStyle.top = domPos.y+10 + 'px';
            graph.labels[nodeId].innerText = graph.getNode(nodeId).data.n;
            document.getElementById('render').appendChild(graph.labels[nodeId])
        }else {
            labelStyle = null
        }
        //section end
    })
}

function webAlign() {
    appendLabel();
}

// graph.label Object에 있는 label들을 html에 append 해 주는 함수
function appendLabel() {
    webglGraphics.placeNode(function (ui, pos) {
        var domPos = {
            x : pos.x,
            y : pos.y
        };
        webglGraphics.transformGraphToClientCoordinates(domPos);

        var nodeId = ui.node.id;
        if (graph.labels[nodeId] !== undefined & graph.getNode(nodeId).data !== undefined){
            var labelStyle = graph.labels[nodeId].style;
            labelStyle.left = domPos.x + 'px';
            labelStyle.top = domPos.y+10 + 'px';
            graph.labels[nodeId].innerText = graph.getNode(nodeId).data.n;
            document.getElementById('render').appendChild(graph.labels[nodeId])
        }else {
            labelStyle = null
        }
        // console.log(nodeId)
    })
}

// ?????????????????????
// 코드 분석 다시 해야함
function addLabel(node) {
    // console.log(node)
    if(typeof(node.id)=="number"){
        node.id = node.id;
    }
    if (Object.keys(graph.labels).indexOf(node.id) !== -1){
        return null;
    }else{
        graph.labels[node.id] = document.createElement('label');
        graph.labels[node.id].classList.add('node-label');
    }
}

// primary node에 label붙이는 부분.
function addPLabel(node) {
    console.log(node)
    node.id = node.i;
    if (Object.keys(graph.labels).indexOf(node.i) !== -1){
        return null;
    }else{
        graph.labels[node.id] = document.createElement('label');
        graph.labels[node.id].classList.add('node-label');
    }
}

